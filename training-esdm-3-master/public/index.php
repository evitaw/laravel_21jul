<?php
require __DIR__ . '/../src/Hello.php';
require __DIR__ . '/../src/BasketBall.php';
require __DIR__ . '/../src/Numerica.php';

use App\Hello;
use App\BasketBall;
use App\Numerica;

$hello = new Hello();
$basket = new BasketBall('Chicago Bulls','Miami Heat');
$numerik = new Numerica();

echo $hello->say();
echo "<br><br>Soal Basket<br>";
echo $basket->home();
echo "<br>";
echo $basket->homeScore();
echo "<br>";
echo $basket->homeThreePoints();
echo "<br>";
echo $basket->away();
echo "<br>";
echo $basket->awayScore();
echo "<br>";
echo $basket->awayTwoPoints();
echo "<br>";
echo $basket->awayOnePoint();
echo "<br>";
echo $basket->homeOnePoint();
echo $numerik->first();
echo $numerik->last();
echo $numerik->min();
echo $numerik->max();
echo $numerik->sum();
echo $numerik->rsort();
echo $numerik->sort();
echo $numerik->odd();
echo $numerik->even();
echo $numerik->greaterThan();
echo $numerik->greaterThanEqual();
echo $numerik->lessThan();
echo $numerik->lessThanEqual();
//echo $numerik->primes();
