<?php

namespace App;

class Numerica
{
    public function first(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $first = $numbers[0];
        echo "<br><br>Soal Numerica <br>" . $first . "<br>";
    }

    public function last(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $last = $numbers[9];
        echo $last . "<br>";
    }

    public function min(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $min = min($numbers);
        echo $min . "<br>";
    }

    public function max(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $max = max($numbers);
        echo $max . "<br>";
    }

    public function sum(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $sum = array_sum($numbers);
        echo $sum . "<br>";
    }

    public function rsort(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $rsort = rsort($numbers);

        $arrlength = count($numbers);
            for($x = 0; $x < $arrlength; $x++) {
            echo $numbers[$x]." ";
            }
            echo "<br>";
    }

    public function sort(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $sort = sort($numbers);
        $arrlength = count($numbers);
            for($x = 0; $x < $arrlength; $x++) {
            echo $numbers[$x]." ";
            }
        echo "<br>";
    }

    public function odd(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $sort = sort($numbers);
        $arrlength = count($numbers);
            for($x = 0; $x <= $arrlength; $x++) {
            if ($x%2!=0)
            echo $x. " ";
            }
        echo "<br>";
    }

    public function even(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $sort = sort($numbers);
        $arrlength = count($numbers);
            for($x = 1; $x <= $arrlength; $x++) {
            if ($x%2==0)
            echo $x. " ";
            }
        echo "<br>";
    }

    public function greaterThan(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $sort = sort($numbers);
        $arrlength = count($numbers);
            for($x = 0; $x <= $arrlength; $x++) {
            if ($x>5)
            echo $x. " ";
            }
        echo "<br>";
    }

    public function greaterThanEqual(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $sort = sort($numbers);
        $arrlength = count($numbers);
            for($x = 0; $x <= $arrlength; $x++) {
            if ($x>=5)
            echo $x. " ";
            }
        echo "<br>";
    }

    public function lessThan(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $sort = sort($numbers);
        $arrlength = count($numbers);
            for($x = 1; $x <= $arrlength; $x++) {
            if ($x<5)
            echo $x. " ";
            }
        echo "<br>";
    }

    public function lessThanEqual(){
        $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
        $sort = sort($numbers);
        $arrlength = count($numbers);
            for($x = 1; $x <= $arrlength; $x++) {
            if ($x<=5)
            echo $x. " ";
            }
        echo "<br>";
    }

    // public function primes(){
    //     $numbers = ([3, 4, 9, 6, 8, 7, 5, 1, 10, 2]);
    //     $sort = sort($numbers);
    //     $arrlength = count($numbers);
    //         for($x = 2; $x < $arrlength; $x++) {
    //             if ($arrlength % $x!=0)
    //                 echo $x. " ";
    //         }
    //     echo "<br>";
    // }
}
