<?php

namespace App;

class BasketBall
{
    public $home='Chicago Bulls';
    public $away='Miami Heat';
    public $homeScore = 0;
    public $awayScore = 0;

    public function home(){
        return $this->home;
    }

    public function homeScore(){
        return $this->homeScore;
    }

    public function homeThreePoints(){
        return $this->homeScore+=3;
    }

    public function away(){
        return $this->away;
    }

    public function awayScore(){
        return $this->awayScore;
    }
    
    public function awayTwoPoints(){
        return $this->awayScore+=2;
    }
    
    public function awayOnePoint(){
        return $this->awayScore+=1;
    }
    
    public function homeOnePoint(){
        return $this->homeScore+=1;
    }
}