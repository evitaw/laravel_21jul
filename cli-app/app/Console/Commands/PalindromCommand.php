<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PalindromCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'palindrom {kalimat=saya ke pasar}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $kalimat = $this->argument('kalimat');
$kecil = strtolower($kalimat);
$nomark = preg_replace('/[^a-zA-Z0-9 -]/', '', $kecil);
$nospace = preg_replace('/\s+/', '', $nomark);
$reverse = strrev($nospace);

if($nospace == $reverse) {
    $this->line('true');
}
else {
    $this->line('false');
}
    }
}
