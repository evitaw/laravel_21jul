<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use voku\helper\HtmlDomParser;

class HeadlineCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'headline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $html = HtmlDomParser::file_get_html('https://www.kompas.com/');

$elements = $html->find('.regional .media__title');

foreach($elements as $element) {
    $href = $element->findOne('a')->getAttribute('href');
    $text = $element->findOne('a')->text();
    $this->line($href);
    $this->line($text);
}

    }
}
