<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MatikCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'matik {angka*}
    {--tambah}
    {--kurang}
    {--kali}
    {--bagi}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $matik = $this->argument('angka');
        $arr_length = count($matik);
        $tambah = $this->option('tambah');
        $kurang = $this->option('kurang');
        $kali = $this->option('kali');
        $bagi = $this->option('bagi');
        if($tambah){
        for($i=1;$i<$arr_length;$i++){
            $matik[$i]+=$matik[$i-1];
            $total=$matik[$i];
        }
        $this->line($total);
        }
        else if($kurang){
        for($j=1;$j<$arr_length;$j++){
            $matik[$j]=$matik[$j-1]-$matik[$j];
            $kurang=$matik[$j];
        }
        $this->line($kurang);}
        else if($kali){
            for($k=1;$k<$arr_length;$k++){
                $matik[$k]*=$matik[$k-1];
                $kali=$matik[$k];
            }
            $this->line($kali);}
            else if($bagi){
                for($l=1;$l<$arr_length;$l++){
                    $matik[$l]=$matik[$l-1]/$matik[$l];
                    $bagi=$matik[$l];
                }
                $this->line($bagi);}
    }
}
