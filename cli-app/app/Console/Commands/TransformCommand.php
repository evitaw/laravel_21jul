<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TransformCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transform 
                            {kalimat=I aM CrAzY TeXT}
                            {--uppercase}
                            {--lowercase}
                            {--ucwords}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $kalimat = $this->argument('kalimat');
        $uppercase = $this->option('uppercase');
        $lowercase = $this->option('lowercase');
        $ucwords = $this->option('ucwords');
        if ($uppercase) {
            $this->line(strtoupper($kalimat));
        } else if($lowercase){
            $this->line(strtolower($kalimat));
        }
        else if($ucwords){
            $kalimat2 = strtolower($kalimat);
            $this->line(ucwords($kalimat2));
        }
    }
}
