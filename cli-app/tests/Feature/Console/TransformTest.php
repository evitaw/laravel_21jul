<?php

namespace Tests\Feature\Console;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransformTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_transform_besar()
    {
        $this->artisan('transform --uppercase')->expectsOutput('I AM CRAZY TEXT');
    }
    public function test_transform_kecil()
    {
        $this->artisan('transform --lowercase')->expectsOutput('i am crazy text');
    }
    public function test_transform_camel()
    {
        $this->artisan('transform --ucwords')->expectsOutput('I Am Crazy Text');
    }
}
