<?php

namespace Tests\Feature\Console;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MatikTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_tambah()
    {
        $this->artisan('matik 1 2 --tambah')->expectsOutput('3');
    }
    public function test_kurang()
    {
        $this->artisan('matik 2 1 --kurang')->expectsOutput('1');
    }
    public function test_kali()
    {
        $this->artisan('matik 1 2 --kali')->expectsOutput('2');
    }
    public function test_bagi()
    {
        $this->artisan('matik 4 2 --bagi')->expectsOutput('2');
    }
}
