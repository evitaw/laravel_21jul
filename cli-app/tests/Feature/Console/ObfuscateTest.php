<?php

namespace Tests\Feature\Console;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ObfuscateTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_obfus()
    {
        $this->artisan('obfus email@example.com')->expectsOutput('eyJpdiI6InIrU3Z3L3J2UWQ3STZWRGxVTjYvL3c9PSIsInZhbHVlIjoiTGplVDFwMHFxUnM0R3Awd096eVFkMjRSZ1dSNjdHbWpKS1IrZ3hTeisvZz0iLCJtYWMiOiI5OGFjMjBhMjAyNmIzMmE5YzNkMjZkNWI4ZTIwZTk1NWM4NTU3ZjdlMzNiODQyNmM5MjZiNjc1MzI2MjAzMDEzIn0=');
    }
}
