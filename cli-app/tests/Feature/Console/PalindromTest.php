<?php

namespace Tests\Feature\Console;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PalindromTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_palindrom()
    {
        $this->artisan('palindrom ibu')->expectsOutput('false');
    }
}
