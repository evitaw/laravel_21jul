<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

// Artisan::command('hello', function() {

// // $name = $this->ask('Nama?');
// // $this->line('Nama adl '.$name);

// // $password = $this->secret('Password?');
// // $this->line('Password '.$password);

// $confirm = $this->confirm('Lanjut?');
// var_dump($confirm);
// })->purpose('Menampilkan');
